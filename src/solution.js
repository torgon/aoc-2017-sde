const fs = require('fs');

function solutionRound(lengths, inputArray, currentPosition, skipSize) {
  let outputArray = inputArray.slice();

  lengths.forEach(length => {
    let subArray = getSubArray(outputArray, length, currentPosition);
    subArray.reverse();

    outputArray = insertSubArray(outputArray, subArray, currentPosition);

    currentPosition = getNewPosition(currentPosition, length, skipSize, outputArray.length);
    skipSize++;

    // fs.appendFileSync('nefunguje.log', inputArray);
    // fs.appendFileSync('nefunguje.log', `\n\n`);
  });

  return {
    outputArray,
    currentPosition,
    skipSize
  };
}

function solution(input, lengths) {
  let inputArray = input.slice();

  let currentPosition = 0;
  let skipSize = 0;

  inputArray = solutionRound(lengths, inputArray, currentPosition, skipSize).outputArray;

  return inputArray[0] * inputArray[1];
}

function solutionRounds(input, lengths) {
  let inputArray = input.slice();

  let currentPosition = 0;
  let skipSize = 0;
  const roundCount = 64;

  for (let i = 0; i < roundCount; i++) {
    let result = solutionRound(lengths, inputArray, currentPosition, skipSize);

    inputArray = result.outputArray;
    currentPosition = result.currentPosition;
    skipSize = result.skipSize;
  }

  return inputArray;
}

function process(input, lengths) {
  let asciiLengths = processToAscii(lengths);

  let result = solutionRounds(input, asciiLengths);

  return xorItAll(result, 16).map(val => val.toString(16).padStart(2, "0")).join("");
}

function xorBlock(inputArray) {
  return inputArray.reduce(function (acc, value) {
    return acc ^ value;
  }, 0);
}

function xorItAll(input, size) {
  let slicedInput = input.slice();
  let result = [];

  while(slicedInput.length > 0) {
    result.push(xorBlock(slicedInput.splice(0, size)));
  }

  return result;
}

function getNewPosition(currentIndex, length, skipSize, arraySize) {
  return ((currentIndex + length + skipSize) % arraySize);
}

function getSubArray(input, length, currentPosition) {
  let subArray = [];

  if (currentPosition + length < input.length) {
    subArray = input.slice(currentPosition, currentPosition +  length);
  } else {
    const firstHalf = input.slice(currentPosition);
    const secondHalf = input.slice(0, length - firstHalf.length);

    subArray = firstHalf.concat(secondHalf);
  }

  return subArray;
}

function insertSubArray(inputArray, reversed, currentPosition) {
  let inputCopy = inputArray.slice();

  if (currentPosition + reversed.length < inputCopy.length) {
    inputCopy.splice(currentPosition, reversed.length, ...reversed);
  } else {
    const firstHalf = reversed.slice(0, inputCopy.length - currentPosition);
    const theOtherHalf = reversed.slice(inputCopy.length - currentPosition);

    inputCopy.splice(currentPosition, firstHalf.length, ...firstHalf);
    inputCopy.splice(0, theOtherHalf.length, ...theOtherHalf);
  }

  return inputCopy;
}

function processToAscii(lengths) {
  let output = [];

  for (let index in lengths) {
    output.push(lengths.charCodeAt(index));
  }

  return output.concat([17, 31, 73, 47, 23]);
}

module.exports = {
  solve: solution,
  getSubArray,
  insertSubArray,
  getNewPosition,
  processToAscii,
  xorBlock,
  xorItAll,
  process
};