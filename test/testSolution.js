const solution = require('../src/solution');

const assert = require('chai').assert;


describe('Test task', function() {
  const INPUT = [0, 1, 2, 3, 4];
  const LENGTHS = [3, 4, 1, 5];

  it('returns 12', function() {
    assert.equal(solution.solve(INPUT, LENGTHS), 12);
  });

  it('returns 1980', function() {
    const lengths = [187,254,0,81,169,219,1,190,19,102,255,56,46,32,2,216];

    let input = [];
    for (let i = 0; i < 256; i++) {
      input[i] = i;
    }

    assert.equal(solution.solve(input, lengths), 1980);
  });

  it('returns vasek number - 5577', function() {
    const lengths = [102,255,99,252,200,24,219,57,103,2,226,254,1,0,69,216];

    let input = [];
    for (let i = 0; i < 256; i++) {
      input[i] = i;
    }

    assert.equal(solution.solve(input, lengths), 5577);
  });
});

describe('getSubArray', function() {
  it('returns first element', function() {
    const result = solution.getSubArray([0, 1], 1, 0);
    assert.deepEqual(result, [0]);
  });

  it('returns first two elements', function() {
    const result = solution.getSubArray([0, 1, 2], 2, 0);
    assert.deepEqual(result, [0, 1]);
  });

  it('returns first two elements', function() {
    const result = solution.getSubArray([0, 1, 2], 2, 1);
    assert.deepEqual(result, [1, 2]);
  });

  it('returns fail', function() {
    const result = solution.getSubArray([0, 1, 2, 3, 4, 5], 2, 3);
    assert.deepEqual(result, [3, 4]);
  });

  it('returns second, third and first element', function() {
    const result = solution.getSubArray([0, 1, 2], 3, 1);
    assert.deepEqual(result, [1, 2, 0]);
  });

  it('returns empty array', function() {
    const result = solution.getSubArray([0, 1, 2], 0, 1);
    assert.deepEqual(result, []);
  });
});

describe('insertSubArray', function() {
  it('insert subArray', function() {
    const result = solution.insertSubArray([0, 1], [1], 0);
    assert.deepEqual(result, [1, 1]);
  });

  it('insert subArray 2', function() {
    const result = solution.insertSubArray([0, 1, 2], [3, 4], 2);
    assert.deepEqual(result, [4, 1, 3]);
  });

  it('insert subArray 3', function() {
    const result = solution.insertSubArray([0, 1, 2, 3, 4, 5], [9, 8, 7, 6], 4);
    assert.deepEqual(result, [7, 6, 2, 3, 9, 8]);
  });

  it('insert of the empty array does not alter input', function() {
    const result = solution.insertSubArray([0, 1], [], 1);
    assert.deepEqual(result, [0, 1]);
  });
});

describe('getNewPosition()', function() {
  it('returns 3', function() {
    const result = solution.getNewPosition(0, 3, 0, 5);
    assert.equal(result, 3);
  });

  it('also returns 3', function() {
    const result = solution.getNewPosition(3, 4, 1, 5);
    assert.equal(result, 3);
  });

  it('returns 1', function() {
    const result = solution.getNewPosition(3, 1, 2, 5);
    assert.equal(result, 1);
  });

  it('returns 4', function() {
    const result = solution.getNewPosition(1, 5, 3, 5);
    assert.equal(result, 4);
  });

  it('returns 0 for double jump', function() {
    const result = solution.getNewPosition(4, 4, 7, 5);
    assert.equal(result, 0);
  });
});

describe('convert lengths to ascii', function() {
  it('converts single character', function() {
    const result = solution.processToAscii('1');

    assert.deepEqual(result, [49, 17, 31, 73, 47, 23]);
  });

  it('converts to characters', function() {
    const result = solution.processToAscii('1,2,3');

    assert.deepEqual(result, [49,44,50,44,51,17,31,73,47,23]);
  });

  it('converts empty string', function() {
    const result = solution.processToAscii('');

    assert.deepEqual(result, [17, 31, 73, 47, 23]);
  });
});

describe('XOR blocks', function () {
  it('xor small array', function () {
    let inputArray = [1, 2, 4];
    let result = solution.xorBlock(inputArray);

    assert.equal(result, 7);
  });

  it('xor small array', function () {
    let inputArray = [1, 2, 3];
    let result = solution.xorBlock(inputArray);

    assert.equal(result, 0);
  });
});

describe('XOR it all', function () {
  it('xor big array', function () {
    let inputArray = [1, 2, 4, 1, 2, 3];
    let result = solution.xorItAll(inputArray, 3);

    assert.deepEqual(result, [7, 0]);
  });

  it('xor big array', function () {
    let inputArray = [1, 2, 4, 8, 1, 2, 3, 4];
    let result = solution.xorItAll(inputArray, 4);

    assert.deepEqual(result, [15, 4]);
  });

});

describe('process', function () {
  let input = [];
  for (let i = 0; i < 256; i++) {
    input[i] = i;
  }

  it('empty string', function () {
    let result = solution.process(input, "");

    assert.equal(result, "a2582a3a0e66e6e86e3812dcb672a272");
  });

  it('AoC 2017 string', function () {
    let result = solution.process(input, "AoC 2017");

    assert.equal(result, "33efeb34ea91902bb2f59c9920caa6cd");
  });

  it('1,2,3 string', function () {
    let result = solution.process(input, "1,2,3");

    assert.equal(result, "3efbe78a8d82f29979031a4aa0b16a9d");
  });

  it('1,2,4 string', function () {
    let result = solution.process(input, "1,2,4");

    assert.equal(result, "63960835bcdc130f0b66d7ff4f6a5a8e");
  });

  it('Michal string', function () {
    let result = solution.process(input, "187,254,0,81,169,219,1,190,19,102,255,56,46,32,2,216");

    assert.equal(result, "899124dac21012ebc32e2f4d11eaec55");
  });
});
